import React from 'react'
import { View, StyleSheet } from 'react-native'

export default props => {
    return (
        <View style={styles.container}>
            <View style={styles.flagpole} />
            <View style={styles.flag} />
            <View style={styles.base1} />
            <View style={styles.base2} />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        marginTop: 2,
      
    },
    flagpole: {
        position: 'absolute',
        height: 33,
        width: 5,
        backgroundColor: '#222',
        marginLeft: 20,
    },
    flag: {
        position: 'absolute',
        height: 15,
        width: 19,
        backgroundColor: '#F22',
        marginLeft: 3,
    },
    base1: {
        position: 'absolute',
        height: 4,
        width: 18,
        backgroundColor: '#222',
        marginLeft: 14,
        marginTop: 29
    },
    base2: {
        position: 'absolute',
        height: 4,
        width: 30,
        backgroundColor: '#222',
        marginLeft: 8,
        marginTop: 33
    },
})