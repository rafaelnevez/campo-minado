import React from 'react'
import { View, StyleSheet } from 'react-native'

export default props => {
    return (
        <View style={styles.container}>
            <View style={styles.coreMiner}/>
            <View style={styles.line}/>
            <View style={[styles.line, {transform:[{rotate: '45deg'}]}]}/>
            <View style={[styles.line, {transform:[{rotate: '90deg'}]}]}/>
            <View style={[styles.line, {transform:[{rotate: '135deg'}]}]}/>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        alignItems:'center',
        justifyContent:'center',
    },
    corMine:{
        height:14,
        width:14,
        borderRadius:10,
        backgroundColor:'black',
        alignItems:'center',
        justifyContent:'center',
    },
    line:{
        backgroundColor:'black',
        borderRadius:3,
        width:20,
        height:3,
        position:'absolute',

    }
})