import { Dimensions, Alert } from 'react-native'

const params = {
    blockSize:50,
    borderSize:5,
    fontSize:30,
    hearderRatio:0.15, //Propoçao da tela superior
    difficultLevel: 0.10,
    getColumnsAmount(){
        const width = Dimensions.get('window').width
        return Math.floor(width/this.blockSize)
    },
    getRowsAmount(){
        const totalHeight = Dimensions.get('window').height
        const boardHeight = totalHeight * (1-this.hearderRatio)
        return Math.floor(boardHeight/this.blockSize)
    }
}

export default params